---
license: other
---


![Aquila_logo](./log.jpeg)


<h4 align="center">
    <p>
        <a href="https://huggingface.co/BAAI/AquilaChat2-34B-16K/blob/main/README.md">English</a> 
        <b>简体中文</b> |
    </p>
</h4>


<p align="center">
  <a href="https://github.com/FlagAI-Open/Aquila2" target="_blank">Github</a> • <a href="https://github.com/FlagAI-Open/Aquila2/blob/main/assets/wechat-qrcode.jpg" target="_blank">WeChat</a> <br>
</p>

# 悟道·天鹰（Aquila2）

我们开源了我们的 **Aquila2** 系列，现在包括基础语言模型 **Aquila2-7B** 和 **Aquila2-34B** ，对话模型 **AquilaChat2-7B** 和 **AquilaChat2-34B**，长文本对话模型**AquilaChat2-7B-16k** 和 **AquilaChat2-34B-16k**

2023.10.25 🔥 基于AquilaChat2-34B-16K初始版本的开发经验，我们对AquilaChat2-34B-16K进行了全面升级并发布1.2版本。
其中AquilaChat2-34B-16K-V1.2相较于V1版本在长文本综合能力上有明显提升，接近GPT-3.5-16K。同时V1.2版本应用了更多的常规指令微调语料，
使其在非长文本场景下的性能也优于V1版本。

悟道 · 天鹰 Aquila 模型的更多细节将在官方技术报告中呈现。请关注官方渠道更新。

## 快速开始使用 AquilaChat2-34B-16K


## 使用方式/How to use

### 1. 推理/Inference

```python
from transformers import AutoTokenizer, AutoModelForCausalLM
import torch
device = torch.device("cuda")
model_info = "BAAI/AquilaChat2-34B-16K"
tokenizer = AutoTokenizer.from_pretrained(model_info, trust_remote_code=True)
model = AutoModelForCausalLM.from_pretrained(model_info, trust_remote_code=True)
model.eval()
model.to(device)
text = "请给出10个要到北京旅游的理由。"
tokens = tokenizer.encode_plus(text)['input_ids']
tokens = torch.tensor(tokens)[None,].to(device)
stop_tokens = ["###", "[UNK]", "</s>"]
with torch.no_grad():
    out = model.generate(tokens, do_sample=True, max_length=512, eos_token_id=100007, bad_words_ids=[[tokenizer.encode(token)[0] for token in stop_tokens]])[0]
    out = tokenizer.decode(out.cpu().numpy().tolist())
    print(out)
```


## 证书/License

Aquila2系列开源模型使用 [智源Aquila系列模型许可协议](https://huggingface.co/BAAI/AquilaChat2-34B-16K/blob/main/BAAI-Aquila-Model-License%20-Agreement.pdf)